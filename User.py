class User:
    def __init__(self, firstName, lastName, birthday, company, email, phoneNumber):
        self.firstName = firstName
        self.lastName = lastName
        self.birthday = birthday
        self.company = company
        self.email = email
        self.phoneNumber = phoneNumber
