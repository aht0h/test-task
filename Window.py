import re
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
from User import *


class Window:


    def __init__(self, master):
        self.users = []
        self.tree = ttk.Treeview(master, height=25)
        self.add_entries = [tk.Entry(master, width=15) for _ in range(6)]
        self.search_entry = tk.Entry(master, width=15)
        self.master = master
        self.master.title("Main Page")
        self.master.geometry('1400x650')
        self.master.config(bg="white")
        self.setup_view()

        self.counter = 0

    def next_id(self):
        self.counter += 1
        return str(self.counter)

    def reset(self):
        self.counter = 0

    def add(self):
        if re.match("^[a-zA-Zа-яА-Я]+$", self.add_entries[0].get()):
            self.firstName = self.add_entries[0].get()
        else:
            messagebox.showerror("Неправильное имя", "Имя введено некорректно")
            self.clear_entries()
            return

        if re.match("^[a-zA-Zа-яА-Я]+$", self.add_entries[1].get()):
            self.lastName = self.add_entries[1].get()
        else:
            messagebox.showerror("Неправильная фамилия", "Фамилия введена некорректно")
            self.clear_entries()
            return

        if re.match("^([1-9]|0[1-9]|1[0-9]|2[0-9]|3[0-1])(\.|-|/)([1-9]|0[1-9]|1[0-2])(\.|-|/)([0-9][0-9]|19[0-9][0-9]|20[0-9][0-9])$", self.add_entries[2].get()):
            self.date = self.add_entries[2].get()
        else:
            messagebox.showerror("Неправильная дата рождения", "Дата рождения может быть в одном из трех форматов: 14-11-2017 14.11.2017 14/11/2017")
            self.clear_entries()
            return

        self.company = self.add_entries[3].get()

        if re.match("^[a-z0-9]+[._]?[a-z0-9]+[@]\w+[.]\w{2,3}$", self.add_entries[4].get()):
            self.email = self.add_entries[4].get()
        else:
            messagebox.showerror("Неправильный email", "Неправильный формат почты")
            self.clear_entries()
            return

        self.phoneNumber = self.add_entries[5].get()

        self.users.append(User(self.firstName, self.lastName, self.date, self.company, self.email, self.phoneNumber))
        self.tree.insert("", tk.END, id=self.next_id(), values=(self.firstName, self.lastName, self.date, self.company, self.email, self.phoneNumber))

        self.clear_entries()

    def backspace_clicked(self, event):
        input = self.add_entries[5]

        if len(str(input.get())) <= 4:
            input.delete(0, tk.END)
            input.insert(0, "+7 (")
        if str(input.get()[-1]) == ")" or str(input.get()[-1]) == "-":
            txt = input.get()[:-1]
            input.delete(0, tk.END)
            input.insert(0, txt)

    def on_focus(self, event):
        input = self.add_entries[5]
        input.delete(0, tk.END)
        input.insert(0, "+7 (")

    def on_press(self, event):
        input = self.add_entries[5]
        if len(str(input.get())) == 2 or len(str(input.get())) == 3 or len(str(input.get())) == 4:
            input.delete(0, tk.END)
            input.insert(0, "+7 (")

    def on_input_number(self, event):
        input = self.add_entries[5]

        if not str(input.get()[-1]).isdigit() or len(str(input.get())) > 17:
            txt = input.get()[:-1]
            input.delete(0, tk.END)
            input.insert(0, txt)

        if len(str(input.get())) == 7:
            input.insert(7, ")")
        if len(str(input.get())) == 11:
            input.insert(11, "-")
        if len(str(input.get())) == 14:
            input.insert(14, "-")

    def clear_entries(self):
        for entry in self.add_entries:
            entry.delete(0, 'end')

    def refresh_tree(self):
        self.tree.delete(*self.tree.get_children())
        self.reset()

        for user in self.users:
            if user is not None:
                self.tree.insert("", tk.END, id=self.next_id(), values=(user.firstName, user.lastName, user.birthday, user.company, user.email, user.phoneNumber))


    def search(self):
        query = self.search_entry.get()
        selections = []
        not_selected = []

        for child in self.tree.get_children():
            for value in self.tree.item(child)['values'][:2]:
                if query.lower() in value.lower():  # compare strings in  lower cases.
                    print(self.tree.item(child)['values'])
                    selections.append(child)
                    break
            for value in self.tree.item(child)['values'][4:]:
                if query.lower() in value.lower():  # compare strings in  lower cases.
                    print(self.tree.item(child)['values'])
                    selections.append(child)
                    break

            if child not in selections:
                not_selected.append(child)
        print('search completed')
        self.tree.selection_set(selections)
        for child_to_del in not_selected:
            self.tree.delete(child_to_del)
        print(self.users)

    def edit(self, event):
        if self.tree.identify_region(event.x, event.y) == 'cell':
            # the user clicked on a cell

            def ok(event):
                """Change item value."""
                self.tree.set(item, column, entry.get())
                entry.destroy()

            column = self.tree.identify_column(event.x)  # identify column
            item = self.tree.identify_row(event.y)  # identify item
            x, y, width, height = self.tree.bbox(item, column)
            value = self.tree.set(item, column)

            entry = ttk.Entry(self.tree)  # create edition entry
            entry.place(x=x, y=y, width=width, height=height + 2,
                        anchor='nw')  # display entry on top of cell
            entry.insert(0, value)  # put former value in entry
            entry.bind('<Return>', ok)  # validate with Enter
            entry.focus_set()

    def delete(self):
        try:
            selected_item = self.tree.selection()[0]
            print(selected_item)
            index = int(selected_item) - 1
            print(index)
            #self.tree.delete(selected_item)
            self.users.pop(index)
            self.refresh_tree()
        except:
            print("remove problem")

    def setup_view(self):
        columns = ["Имя", "Фамилия", "Дата рождения", "Компания", "Почта", "Номер телефона"]
        self.tree["columns"] = ["0", "1", "2", "3", "4", "5"]
        for i in range(6):
            self.tree.column(str(i), width=120)
            self.tree.heading(str(i), text=columns[i])
        self.tree["show"] = "headings"
        self.tree.grid(row=0, column=2, rowspan=6, pady=20)
        self.tree.bind('<Double-Button-1>', self.edit)

        lb1 = tk.Label(self.master, text="Найти:")
        lb1.grid(row=0, column=0, padx=10, pady=10, sticky=tk.W)
        self.search_entry.grid(row=0, column=1, padx=10, pady=10, sticky=tk.E, rowspan=1)
        btn = tk.Button(self.master, text="Найти", width=10, command=self.search)
        btn.grid(row=0, column=0, padx=10, pady=10, rowspan=2)

        showallbtn = tk.Button(self.master, text="Показать все контакты", width=20, command=self.refresh_tree)
        showallbtn.grid(row=1, column=0, padx=10, pady=10, rowspan=2)

        self.add_lbs = [tk.Label(self.master, text=columns[i]) for i in range(6)]
        for i in range(6):
            self.add_lbs[i].grid(row=i, column=3, padx=10, sticky=tk.W)
            self.add_entries[i].grid(row=i, column=4, padx=10, sticky=tk.E)
        self.add_entries[5].bind("<KeyPress>", self.on_press)
        self.add_entries[5].bind("<KeyRelease>", self.on_input_number)
        self.add_entries[5].bind("<FocusIn>", self.on_focus)
        self.add_entries[5].bind("<BackSpace>", self.backspace_clicked)
        btn1 = tk.Button(self.master, text="Добавить", width=10, command=self.add)
        btn1.grid(row=7, column=3, padx=10, pady=10)

        del_btn = tk.Button(self.master, text="Удалить", width=10, command=self.delete)
        del_btn.grid(row=7, column=2, sticky=tk.N)

        # edit_btn = tk.Button(self.master, text="Изменить", width=10)
        # edit_btn.grid(row=8, column=2, sticky=tk.N)
        # i = 1
        # for entry in self.add_entries:
        #     entry.grid(row=i, column=1, sticky=tk.N)
        #     i += 1
        # btn1 = tk.Button(self.master, text="Добавить", width=10, command=self.add)
        # btn1.grid(row=1, column=0, padx=10, pady=10, rowspan=2)

